package com.iamb.media.android.models;

/**
 * Created by andrei.boleac on 13/07/15.
 */
public class NavDrawerModel {
    private String navItemName;
    private int itemIconResourceId;
    private int itemIconResourceIdWhite;
    private String navItemContent = "";
    private boolean hasDivider;

    public String getNavItemName() {
        return navItemName;
    }

    public void setNavItemName(String navItemName) {
        this.navItemName = navItemName;
    }

    public int getItemIconResourceId() {
        return itemIconResourceId;
    }

    public void setItemIconResourceId(int itemIconResourceId) {
        this.itemIconResourceId = itemIconResourceId;
    }

    public boolean hasDivider() {
        return hasDivider;
    }

    public void setHasDivider(boolean hasDivider) {
        this.hasDivider = hasDivider;
    }

    public String getNavItemContent() {
        return navItemContent;
    }

    public void setNavItemContent(String navItemContent) {
        this.navItemContent = navItemContent;
    }

    public int getItemIconResourceIdWhite() {
        return itemIconResourceIdWhite;
    }

    public void setItemIconResourceIdWhite(int itemIconResourceIdWhite) {
        this.itemIconResourceIdWhite = itemIconResourceIdWhite;
    }
}
