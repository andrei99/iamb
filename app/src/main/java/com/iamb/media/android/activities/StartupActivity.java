package com.iamb.media.android.activities;

import android.content.Intent;
import android.os.Bundle;

import com.iamb.media.android.utils.UserUtils;

/**
 * Created by andrei.boleac on 07/11/15.
 */
public class StartupActivity extends IambBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (UserUtils.isUserSignedIn(this)) {
            startActivity(getStartActivityIntent(MainActivity.class));
        } else {
            startActivity(getStartActivityIntent(LoginMainActivity.class));
        }
        finish();

    }

    @Override
    String getActivityTitle() {
        return "";
    }

    private Intent getStartActivityIntent(Class activity) {
        Intent intent = new Intent(this, activity);
        return intent;
    }


}
