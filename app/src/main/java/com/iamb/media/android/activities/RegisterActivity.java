package com.iamb.media.android.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.iamb.media.android.R;
import com.iamb.media.android.customviews.CustomClickableSpan;
import com.iamb.media.android.utils.Constants;
import com.iamb.media.android.utils.Utils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by andrei.boleac on 07/11/15.
 */
public class RegisterActivity extends IambBaseActivity {

    private static final int REQUEST_CODE_CAMERA_PERSMISION = 10;
    @Bind(R.id.first_name)
    EditText firstName;
    @Bind(R.id.last_name)
    EditText lastName;
    @Bind(R.id.email)
    EditText emailInput;
    @Bind(R.id.input_password)
    EditText passwordInput;
    @Bind(R.id.retype_password)
    EditText retypePasswordInput;
    @Bind(R.id.terms_and_conditions)
    TextView termsAndConditions;
    @Bind(R.id.register_btn)
    Button registerBtn;
    @Bind(R.id.select_photo)
    ImageView profilePicture;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        setUpTermsAndConditionsTextView();
    }

    @Override
    String getActivityTitle() {
        return getString(R.string.sign_up_btn);
    }

    private void setUpTermsAndConditionsTextView() {
        String[] wordsToColor = new String[]{getString(R.string.terms_and_conditions)};
        ClickableSpan startTermsAndConditionsScreen = new CustomClickableSpan() {
            @Override
            public void onClick(View widget) {
                startActivity(Utils.createIntentForActivityToStart(RegisterActivity.this, TermsAndConditionsActivity.class));
            }
        };
        ClickableSpan[] actions = new ClickableSpan[]{startTermsAndConditionsScreen};
        termsAndConditions.setText(Utils.getFormattedString(getString(R.string.terms_and_conditions_link), getResources().getColor(android.R.color.holo_blue_dark), wordsToColor, actions), TextView.BufferType.SPANNABLE);
        termsAndConditions.setMovementMethod(LinkMovementMethod.getInstance());

    }

    @OnClick(R.id.register_btn)
    public void registerUser() {
        validateInputData();
    }

    @OnClick(R.id.select_photo)
    public void selectPhoto() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            startImageCapturing();
        } else {
            checkCameraPermision();
        }


    }

    private void startImageCapturing() {
        Intent intent = new Intent(Constants.ACTION_CAPTURE_IMAGE);
        startActivityForResult(intent, Constants.REQUEST_CODE_TAKE_PHOTO);
    }

    private void validateInputData() {
        String emailText = emailInput.getText().toString();
        String passwordText = passwordInput.getText().toString();
        String retypePasswordText = retypePasswordInput.getText().toString();
        String firstNameText = firstName.getText().toString();
        String lastNameText = lastName.getText().toString();
        if (emailText.isEmpty()) {
            emailInput.setError(getString(R.string.email_field_required));
            return;
        }
        if (!Utils.isValidEmailAddress(emailText)) {
            emailInput.setError(getString(R.string.error_invalid_email));
            return;
        }
        if (passwordText.isEmpty()) {
            passwordInput.setError(getString(R.string.password_field_required));
            return;
        }

        if (retypePasswordText.isEmpty()) {
            retypePasswordInput.setError(getString(R.string.password_field_required));
        }

        if (!retypePasswordText.equals(passwordText)) {
            Utils.showToast(this, getString(R.string.retype_password_wrong));
        }
        makeRegistrationRequest(firstNameText, lastNameText, emailText, passwordText);

    }

    private void makeRegistrationRequest(String firstName, String lastName, String email, String password) {

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CODE_TAKE_PHOTO && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get(Constants.KEY_DATA);
            profilePicture.setImageBitmap(imageBitmap);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_CAMERA_PERSMISION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startImageCapturing();
            } else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkCameraPermision() {


        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED)

        {

            requestPermissions(new String[]{Manifest.permission.CAMERA},
                    REQUEST_CODE_CAMERA_PERSMISION);
        } else {
            startImageCapturing();
        }
    }

}
