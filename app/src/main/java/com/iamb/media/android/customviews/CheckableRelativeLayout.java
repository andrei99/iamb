package com.iamb.media.android.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Checkable;
import android.widget.RelativeLayout;

import com.iamb.media.android.R;


/**
 * Created by andrei.boleac on 04/08/15.
 */
public class CheckableRelativeLayout extends RelativeLayout implements
        Checkable {

    public CheckableRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private boolean isChecked = false;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
        changeColor(isChecked);
    }

    public void toggle() {
        this.isChecked = !this.isChecked;
        changeColor(this.isChecked);
    }

    private void changeColor(boolean isChecked) {
        if (isChecked) {
            setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        } else {
            setBackgroundColor(getResources().getColor(android.R.color.white));
        }

    }
}
