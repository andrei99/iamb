package com.iamb.media.android.activities;

import android.os.Bundle;

import com.iamb.media.android.R;
import com.iamb.media.android.utils.Utils;

/**
 * Created by andrei.boleac on 07/11/15.
 */
public class LoginMainActivity extends IambBaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

    }

    @Override
    protected void setUpActionBar() {
    }

    @Override
    String getActivityTitle() {
        return "";
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.hideSystemUI(getWindow());
    }
}
