package com.iamb.media.android.customviews;

import android.support.annotation.NonNull;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

/**
 * Created by andrei.boleac on 13/05/15.
 */
public  class CustomClickableSpan extends ClickableSpan {

    @Override
    public void onClick(View view) {

    }
    @Override
    public void updateDrawState(@NonNull TextPaint ds) {
        ds.setUnderlineText(false); // set to false to remove underline
    }
}
