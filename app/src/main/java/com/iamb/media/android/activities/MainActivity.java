package com.iamb.media.android.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;

import com.iamb.media.android.R;
import com.iamb.media.android.fragments.IABMTvFragment;
import com.iamb.media.android.fragments.NavigationDrawerFragment;
import com.iamb.media.android.fragments.WallFragment;
import com.iamb.media.android.utils.Constants;
import com.iamb.media.android.utils.Utils;

/**
 * Created by andrei.boleac on 07/11/15.
 */
public class MainActivity extends IambBaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private static final String TAG = MainActivity.class.getSimpleName();
    private NavigationDrawerFragment navigationDrawerFragment;
    private Toolbar toolbar;
    private CharSequence currentTitle;
    private int selectedNavDrawerItem = Constants.ITEM_HOME;
    private Fragment fragment;
    private ImageView profileImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        currentTitle = savedInstanceState != null && savedInstanceState.containsKey(Constants.TITLE)
                ? savedInstanceState.getString(Constants.TITLE) : getString(R.string.nav_section_home);
        initToolbar();
        initToolbarProfileImage();
        navigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);
        navigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);


    }

    @Override
    protected void setUpActionBar() {
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(Constants.TITLE, currentTitle.toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        Log.d(TAG, "page title is " + currentTitle);
        restoreActionBar();
        return true;
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(currentTitle);
        navigationDrawerFragment.setItemChecked(selectedNavDrawerItem);
    }

    @Override
    String getActivityTitle() {
        return getString(R.string.main_screen_title);
    }

    private void initToolbar() {
        Log.d(TAG, "init toolbar ");

        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initToolbarProfileImage() {
        this.profileImage = (ImageView) toolbar.findViewById(R.id.profile_image);
        this.profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.showToast(MainActivity.this, "This will open user profile page");
            }
        });

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {

        switch (position) {

            case Constants.ITEM_HOME:
                currentTitle = getString(R.string.nav_section_home_action_bar_title);
                fragment = WallFragment.newInstance(currentTitle.toString());

                break;
            case Constants.ITEM_LIVE_POLLS:
                currentTitle = getString(R.string.nav_section_polls);
                fragment = WallFragment.newInstance(currentTitle.toString());


                break;
            case Constants.ITEM_FOOTSTEPS:
                currentTitle = getString(R.string.nav_section_footsteps);
                fragment = WallFragment.newInstance(currentTitle.toString());

                break;
            case Constants.ITEM_NEWS:
                currentTitle = getString(R.string.nav_section_news);
                fragment = WallFragment.newInstance(currentTitle.toString());

                break;
            case Constants.ITEM_IABM_TV:
                fragment = IABMTvFragment.newInstance();
                currentTitle = getString(R.string.nav_section_iabm_tv);
                break;
            case Constants.ITEM_EVENTS:
                currentTitle = getString(R.string.nav_section_events);
                fragment = WallFragment.newInstance(currentTitle.toString());

                break;
            case Constants.ITEM_Q_A:
                currentTitle = getString(R.string.nav_section_q_a);
                fragment = WallFragment.newInstance(currentTitle.toString());

                break;
            case Constants.ITEM_BARS_HOTELS:
                currentTitle = getString(R.string.nav_section_bars_hotels);
                fragment = WallFragment.newInstance(currentTitle.toString());

                break;
            case Constants.ITEM_MEETING_ROOMS:
                currentTitle = getString(R.string.nav_section_meeting_rooms);
                fragment = WallFragment.newInstance(currentTitle.toString());
                break;
            case Constants.ITEM_MY_PROFILE:
                currentTitle = getString(R.string.nav_section_my_profile);
                fragment = WallFragment.newInstance(currentTitle.toString());
                break;
            case Constants.ITEM_SETTINGS:
                currentTitle = getString(R.string.nav_section_settings);
                fragment = WallFragment.newInstance(currentTitle.toString());

                break;
            case Constants.ITEM_LOGOUT:
                break;
        }

        if (position != Constants.ITEM_LOGOUT) {
            selectedNavDrawerItem = position;
            switchContentFragment(fragment);

        }
        if (profileImage != null) {
            if (position == Constants.ITEM_HOME) {
                profileImage.setVisibility(View.VISIBLE);
            } else {
                profileImage.setVisibility(View.GONE);
            }
        }


    }

    private void switchContentFragment(Fragment fragment) {
        invalidateOptionsMenu();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment).commitAllowingStateLoss();

    }
}
