package com.iamb.media.android.utils;

/**
 * Created by andrei.boleac on 07/11/15.
 */
public class Constants {

    public static final int REQUEST_CODE_TAKE_PHOTO = 0;
    public static final String ACTION_CAPTURE_IMAGE = "android.media.action.IMAGE_CAPTURE";
    public static final String KEY_DATA = "data";

    public static final int ITEM_HOME=0;
    public static final int ITEM_LIVE_POLLS=1;
    public static final int ITEM_FOOTSTEPS=2;
    public static final int ITEM_NEWS=3;
    public static final int ITEM_EVENTS=4;
    public static final int ITEM_IABM_TV=5;
    public static final int ITEM_Q_A=6;
    public static final int ITEM_BARS_HOTELS=7;
    public static final int ITEM_MEETING_ROOMS =8;
    public static final int ITEM_MY_PROFILE=9;
    public static final int ITEM_SETTINGS=10;
    public static final int ITEM_LOGOUT=11;

    public static final String TITLE = "title";
    public static final String PAGE_TITLE = "page_title";
}
