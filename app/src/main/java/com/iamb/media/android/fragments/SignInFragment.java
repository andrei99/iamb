package com.iamb.media.android.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.iamb.media.android.R;
import com.iamb.media.android.activities.LoginIambActivity;
import com.iamb.media.android.activities.MainActivity;
import com.iamb.media.android.utils.Utils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by andrei.boleac on 07/11/15.
 */
public class SignInFragment extends Fragment {

    private static final String TAG = SignInFragment.class.getSimpleName();
    private CallbackManager callbackManager;
    @Bind(R.id.facebook_btn)
    LoginButton facebookLoginBtn;
    @Bind(R.id.app_login)
    Button logInWithEmailBtn;
    @Bind(R.id.skip_container)
    View skipLogin;
    private Activity activity;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpFacebookSdk();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sign, container, false);
        ButterKnife.bind(this, rootView);
        setUpFacebookBtn();
        return rootView;

    }

    private void setUpFacebookBtn() {
        if (facebookLoginBtn != null) {
            facebookLoginBtn.setCompoundDrawables(null, null, null, null);
            facebookLoginBtn.setFragment(this);

        }
    }


    private void setUpFacebookSdk() {
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        registerFacebookCallback();
    }

    private void registerFacebookCallback() {
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        String facebookToken = loginResult.getAccessToken().getToken();
                        Log.d(TAG, "TOKEN IS " + facebookToken);
                    }

                    @Override
                    public void onCancel() {
                        Log.d(TAG, "on cancel ");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.d(TAG, "exception is " + exception.getMessage());
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.app_login)
    public void loginWithIambAccount() {
        Intent startAppLoginIntent = new Intent(activity, LoginIambActivity.class);
        startActivity(startAppLoginIntent);
    }

    @OnClick(R.id.skip_container)
    public void skipLogin() {
        startActivity(Utils.createIntentForActivityToStart(activity, MainActivity.class));
        activity.finish();
    }
}
