package com.iamb.media.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.iamb.media.android.R;
import com.iamb.media.android.models.NavDrawerModel;

import java.util.List;

/**
 * Created by andrei.boleac on 13/07/15.
 */
public class NavigationDrawerItemAdapter extends ArrayAdapter<NavDrawerModel> {

    private final LayoutInflater layoutInflater;
    private final List<NavDrawerModel> list;
    private ListView listView;

    public NavigationDrawerItemAdapter(Context context, List<NavDrawerModel> list, ListView listView) {
        super(context, R.layout.item_nav_drawer, list);
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = list;
        this.listView = listView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final NavDrawerModel navDrawerModel = list.get(position);
        NavDrawerHolder videoHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_nav_drawer, parent, false);
            videoHolder = new NavDrawerHolder();
            videoHolder.navItemName = (TextView) convertView.findViewById(R.id.nav_item_name);
            videoHolder.divider = convertView.findViewById(R.id.nav_drawer_divider);
            videoHolder.navItemIcon = (ImageView) convertView.findViewById(R.id.nav_item_icon);
            videoHolder.rootView = convertView.findViewById(R.id.root_view);


            convertView.setTag(videoHolder);
        } else {
            videoHolder = (NavDrawerHolder) convertView.getTag();
        }
        if (listView.isItemChecked(position)) {
            videoHolder.navItemIcon.setImageResource(navDrawerModel.getItemIconResourceIdWhite());
            videoHolder.navItemName.setTextColor(getContext().getResources().getColor(R.color.white));
        } else {
            videoHolder.navItemIcon.setImageResource(navDrawerModel.getItemIconResourceId());
            videoHolder.navItemName.setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
        }
        videoHolder.navItemName.setText(navDrawerModel.getNavItemName());
        return convertView;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public NavDrawerModel getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getPosition(NavDrawerModel item) {
        return list.indexOf(item);
    }

    private static class NavDrawerHolder {
        public TextView navItemName;
        public ImageView navItemIcon;
        public View divider;
        public View rootView;
    }


}
