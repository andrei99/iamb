package com.iamb.media.android.activities;

import android.os.Bundle;

import com.iamb.media.android.R;

/**
 * Created by andrei.boleac on 07/11/15.
 */
public class TermsAndConditionsActivity extends IambBaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    String getActivityTitle() {
        return getString(R.string.terms_conditions);
    }
}
