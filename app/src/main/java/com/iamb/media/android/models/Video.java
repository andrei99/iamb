package com.iamb.media.android.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by andrei.boleac on 08/11/15.
 */
public class Video implements Parcelable {

    private static final int HASH_CODE_CONSTANT = 31;

    private String videoDescription;
    private String videoUri;
    private int resourceId;
    private String id;

    public Video(){

    }


    private Video(Parcel in) {

        videoDescription = in.readString();
        videoUri = in.readString();
        resourceId = in.readInt();

    }

    public static final Parcelable.Creator<Video> CREATOR = new Parcelable.Creator<Video>() {
        public Video createFromParcel(Parcel in) {
            return new Video(in);
        }

        public Video[] newArray(int size) {
            return new Video[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(videoDescription);
        dest.writeString(videoUri);

        dest.writeInt(resourceId);


    }

    @Override
    public boolean equals(Object o) {

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Video video = (Video) o;

        return !(id != null && !id.equals(video.id));


    }

    @Override
    public int hashCode() {
        int result = 0;
        result = HASH_CODE_CONSTANT * result + videoDescription.hashCode();
        result = HASH_CODE_CONSTANT * result + videoUri.hashCode();
        result = HASH_CODE_CONSTANT * result + id.hashCode();

        return result;
    }

    public String getVideoDescription() {
        return videoDescription;
    }

    public void setVideoDescription(String videoDescription) {
        this.videoDescription = videoDescription;
    }

    public String getVideoUri() {
        return videoUri;
    }

    public void setVideoUri(String videoUri) {
        this.videoUri = videoUri;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
