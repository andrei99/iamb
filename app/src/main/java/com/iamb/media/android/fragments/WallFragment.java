package com.iamb.media.android.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iamb.media.android.R;
import com.iamb.media.android.utils.Constants;

/**
 * Created by andrei.boleac on 08/11/15.
 */
public class WallFragment extends Fragment {

    private String pageTitle;

    public static WallFragment newInstance(String pageTitle) {
        WallFragment fragment = new WallFragment();
        Bundle args = new Bundle();
        args.putString(Constants.PAGE_TITLE, pageTitle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        pageTitle = getArguments().getString(Constants.PAGE_TITLE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_wall, container, false);
        TextView pageTitleView = (TextView) rootView.findViewById(R.id.page_title);
        pageTitleView.setText(pageTitle);

        return rootView;
    }
}
