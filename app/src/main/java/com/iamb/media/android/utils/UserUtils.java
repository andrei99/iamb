package com.iamb.media.android.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by andrei.boleac on 07/11/15.
 */
public class UserUtils {

    public static final String USER_SIGNED_IN = "user_signed_in";

    public static boolean isUserSignedIn(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(USER_SIGNED_IN, false);
    }

    public static void setUserSignedIn(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putBoolean(USER_SIGNED_IN, true).commit();

    }
}
