package com.iamb.media.android.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iamb.media.android.R;
import com.iamb.media.android.models.IABMHeaderModel;
import com.iamb.media.android.models.Video;
import com.iamb.media.android.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by andrei.boleac on 08/11/15.
 */
public class IABMTvDataAdapter extends RecyclerView.Adapter<IABMTvDataAdapter.IAMBTvBaseViewHolder> {

    private static final int ITEM_TYPE_HEADER = 1;
    private static final int ITEM_TYPE_VIEW = 2;
    private Context context;


    private List<Object> dataList = new ArrayList<>();


    public IABMTvDataAdapter(Context context, List<Object> videoList) {
        if (videoList != null) {
            dataList.addAll(videoList);
        }
        this.context = context;
    }


    @Override
    public IAMBTvBaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        switch (viewType) {
            case ITEM_TYPE_VIEW:
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_iabm_tv, parent, false);
                return new IABMTVViewHolder(view);
            case ITEM_TYPE_HEADER:
                View viewHeader = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_header_iabm_tv, parent, false);
                return new IABMTvHeaderViewHolder(viewHeader);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(IAMBTvBaseViewHolder holder, int position) {
        holder.bind(dataList.get(position));

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object object = dataList.get(position);
        if (object instanceof IABMHeaderModel) {
            return ITEM_TYPE_HEADER;
        }
        return ITEM_TYPE_VIEW;

    }

    public class IABMTVViewHolder extends IAMBTvBaseViewHolder {
        private Video video;
        @Bind(R.id.video_description)
        TextView videoDescription;
        @Bind(R.id.play_action)
        ImageView playAction;
        @Bind(R.id.share_action)
        ImageView shareAction;

        public IABMTVViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }

        @OnClick(R.id.play_action)
        public void playVideo() {
            Utils.showToast(context, "Playing video");
        }

        @OnClick(R.id.share_action)
        public void share() {
            Utils.showToast(context, "Sharing video");

        }


        @Override
        void bind(Object object) {
            this.video = (Video) object;

        }
    }

    public class IABMTvHeaderViewHolder extends IAMBTvBaseViewHolder {


        public IABMTvHeaderViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        void bind(Object object) {

        }
    }

    public abstract class IAMBTvBaseViewHolder extends RecyclerView.ViewHolder {

        public IAMBTvBaseViewHolder(View itemView) {
            super(itemView);
        }

        abstract void bind(Object object);


    }
}
