package com.iamb.media.android.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.iamb.media.android.R;
import com.iamb.media.android.adapters.IABMTvDataAdapter;
import com.iamb.media.android.models.IABMHeaderModel;
import com.iamb.media.android.models.Video;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by andrei.boleac on 08/11/15.
 */
public class IABMTvFragment extends Fragment {

    private Activity activity;
    @Bind(R.id.iabm_tv_video_list)
    RecyclerView videoList;
    private IABMTvDataAdapter iabmTvDataAdapter;


    public static IABMTvFragment newInstance() {
        IABMTvFragment fragment = new IABMTvFragment();
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_iabm_tv, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpAdapter();
    }

    private void setUpAdapter() {
        iabmTvDataAdapter = new IABMTvDataAdapter(activity,createMockVideos());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        videoList.setLayoutManager(linearLayoutManager);
        videoList.setAdapter(iabmTvDataAdapter);

    }

    private List<Object> createMockVideos() {
        List<Object> mocks = new ArrayList<>();
        Video video = new Video();
        video.setVideoDescription("IABM annual conference - \"Will we recognize our industry in 10 years time ?");
        video.setVideoUri("should provide an uri here");
        video.setResourceId(R.drawable.iabm_logo_splash);
        IABMHeaderModel iabmHeaderModel = new IABMHeaderModel();
        mocks.add(iabmHeaderModel);
        mocks.add(video);
        mocks.add(video);
        mocks.add(video);
        mocks.add(video);
        mocks.add(video);
        mocks.add(video);
        mocks.add(video);
        mocks.add(video);
        mocks.add(video);
        mocks.add(video);
        mocks.add(video);
        mocks.add(video);
        mocks.add(video);


        return mocks;
    }
}
