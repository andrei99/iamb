package com.iamb.media.android.utils;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.iamb.media.android.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by andrei.boleac on 07/11/15.
 */
public class Utils {

    public static void hideSystemUI(Window window) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                            View.SYSTEM_UI_FLAG_FULLSCREEN |
                            View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            );
        } else {

            window.getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LOW_PROFILE
            );
        }
    }

    public static SpannableStringBuilder getFormattedString(String textToFormat, int color, String[] wordsToColor, ClickableSpan[] actions) {
        final SpannableStringBuilder formattedText = new SpannableStringBuilder(textToFormat);
        for (int i = 0; i < wordsToColor.length; i++) {
            Pattern p = Pattern.compile(wordsToColor[i], Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(formattedText);
            while (m.find()) {
                if (actions != null) {
                    formattedText.setSpan(actions[i], m.start(), m.end(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                }
                formattedText.setSpan(new ForegroundColorSpan(color), m.start(), m.end(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                formattedText.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), m.start(), m.end(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            }
        }


        return formattedText;
    }

    public static boolean isValidEmailAddress(String email) {

        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);

        return m.matches();
    }

    public static Intent createIntentForActivityToStart(Context context, Class activityToStart) {
        return new Intent(context, activityToStart);
    }

    public static void showToast(Context context, String message) {
        if (context != null) {
            Toast.makeText(context.getApplicationContext(), message, Toast.LENGTH_LONG).show();

        }
    }

    public static boolean isTablet(Context context) {
        return context.getResources().getBoolean(R.bool.is_tablet);
    }
}
