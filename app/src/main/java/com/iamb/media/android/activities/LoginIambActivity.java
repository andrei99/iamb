package com.iamb.media.android.activities;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.iamb.media.android.R;
import com.iamb.media.android.customviews.CustomClickableSpan;
import com.iamb.media.android.utils.Utils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by andrei.boleac on 07/11/15.
 */
public class LoginIambActivity extends IambBaseActivity {

    @Bind(R.id.sign_up)
    TextView signUp;
    @Bind(R.id.forgot_passowrd)
    TextView forgotPassword;
    @Bind(R.id.login_btn)
    Button loginBtn;
    @Bind(R.id.input_email)
    EditText email;
    @Bind(R.id.input_password)
    EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_app_account);
        ButterKnife.bind(this);
        setUpSignUpTextView();

    }

    private void setUpSignUpTextView() {
        String[] wordsToColor = new String[]{getString(R.string.sign_up_now)};
        ClickableSpan startRegisterUserScreen = new CustomClickableSpan() {
            @Override
            public void onClick(View widget) {
                startActivity(Utils.createIntentForActivityToStart(LoginIambActivity.this, RegisterActivity.class));
            }
        };
        ClickableSpan[] actions = new ClickableSpan[]{startRegisterUserScreen};
        signUp.setText(Utils.getFormattedString(getString(R.string.sign_up_message), getResources().getColor(android.R.color.holo_blue_dark), wordsToColor, actions), TextView.BufferType.SPANNABLE);
        signUp.setMovementMethod(LinkMovementMethod.getInstance());

    }

    @Override
    String getActivityTitle() {
        return getString(R.string.login_activity_title);
    }

    @OnClick(R.id.forgot_passowrd)
    public void startForgotPasswordScreen() {
        startActivity(Utils.createIntentForActivityToStart(this, ForgotPasswordActivity.class));
    }

    @OnClick(R.id.login_btn)
    public void login() {
        validateInputData();
        String emailText = email.getText().toString();
        String passwordText = password.getText().toString();
        Toast.makeText(this, "Email and password are " + emailText + " " + passwordText, Toast.LENGTH_SHORT).show();


    }

    private void validateInputData() {
        if (email.getText().toString().isEmpty()) {
            email.setError(getString(R.string.email_field_required));
            return;
        }
        if (!Utils.isValidEmailAddress(email.getText().toString())) {
            email.setError(getString(R.string.error_invalid_email));
            return;
        }
        if (password.getText().toString().isEmpty()) {
            password.setError(getString(R.string.password_field_required));
            return;
        }
    }
}
