package com.iamb.media.android.activities;

import android.os.Bundle;

import com.iamb.media.android.R;

import butterknife.ButterKnife;

/**
 * Created by andrei.boleac on 07/11/15.
 */
public class ForgotPasswordActivity extends IambBaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
    }

    @Override
    String getActivityTitle() {
        return "Forgot password";
    }
}
